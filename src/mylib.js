
const mylib = {
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },
    subtract: (a, b) => {
        return a - b;
    },
    divide: (dividend, divisor) => {
        if (divisor === 0) {
            throw new Error('divisor 0 not allowed');
        }
        return dividend / divisor;
    },
    multiply: function(a, b) {
        return a * b;
    }
};

module.exports = mylib;
